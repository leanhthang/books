module ApiResponse
  extend ActiveSupport::Concern

  def render_result(messages = "Successfully", data = {}, is_success = true, status = 200)
    return_json = {}

    return_json[:messages] = messages
    return_json[:is_success] = is_success
    return_json[:code] = status
    return_json[:data] = data if data.present?

    render json: return_json, status: status
  end

  def render_object(message, object, serializer, instance_options = {})
    hash = { success: true, message: message, data: {} }

    if object
      json = serializer.new(object, instance_options).as_json
      hash[:data] = hash[:data].merge(json)
    end

    render json: hash
  end
end
