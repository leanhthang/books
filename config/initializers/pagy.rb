Rails.application.config.assets.paths << Pagy.root.join('javascripts')

require 'pagy/extras/bootstrap'
require 'pagy/extras/i18n'
require 'pagy/extras/navs'

Pagy::VARS[:steps] = { 0 => [1,1,1,1], 640 => [1,2,2,1], 820 => [3,3,3,3] }

