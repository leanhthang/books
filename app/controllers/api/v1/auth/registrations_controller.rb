class Api::V1::Auth::RegistrationsController < DeviseTokenAuth::RegistrationsController
  include ApiMessage
  include ApiResponse
  include ApiHelper

  skip_before_action :verify_authenticity_token
  before_action :ensure_params_exist, only: :create
  before_action :sign_up_params, only: :create

  # sign up
  def create
    reader = Reader.new sign_up_params
    # reader.skip_password_validation = true
    begin
      if reader.save
        data = { reader: reader }
        render_result(api_msg('sign_up.success'), data)
      else
        render_result(reader.errors, {}, false, :unprocessable_entity)
      end
    rescue Exception => e
      render_error(e)
    end
  end

  private
    def sign_up_params
      new_params = params.require(:auth).permit(:login, :provider, :uid)
      # Add device ID
      new_params.merge!({device_id: request.headers['Device-Id'] })

      # Auto fill password
      new_params.merge!({password: '18121990', password_confirmation: '18121990'})

      login_parse = parse_field_login(new_params[:login])
      if login_parse
        new_params.merge!(login_parse)
        new_params.delete(:login)
        new_params
      else
        render_result(api_msg('sign_up.email_phone_missing'), {}, false, :bad_request)
      end
    end

    def ensure_params_exist
      return if params[:auth].present?
      render_result(api_msg('sign_up.missing_params'), {}, false, :bad_request)
    end
end
