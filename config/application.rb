require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Books
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 6.0

    config.require_master_key = true

    # Local vn
    config.time_zone = 'Hanoi'
    config.i18n.default_locale = :vi

    # Use Vips for processing variants.
    config.active_storage.variant_processor = :vips
    config.active_storage.service_urls_expire_in = 1.days

    config.middleware.use Rack::Attack

    config.cache_store = :redis_cache_store, {
            driver: :hiredis,
            host: "localhost",
            port: 6379,
            db: 1,
            namespace: "books_redis",
            size: 500.megabytes,
            expires_in: 7.days
          }

    config.middleware.use Rack::Cors do
      allow do
        origins '*'
        resource '*',
          headers: :any,
          expose: ['access-token', 'expiry', 'token-type', 'uid', 'client'],
          methods: [:get, :post, :options, :delete, :put]
      end
    end


    config.active_job.queue_adapter = :sidekiq
  end
end
