class Book < ApplicationRecord
  include Utility
  extend FriendlyId

  friendly_id :title_search, use: :slugged

  has_and_belongs_to_many :categories
  has_many :chaps, dependent: :delete_all

  belongs_to :author, optional: true
end
