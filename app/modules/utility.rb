module Utility
  def self.selfload_remote_file
    response = Net::HTTP.get_response(URI.parse('https://bachngocsach.com/reader/sites/default/files/anhbia/300_1.jpg'))
    file = StringIO.new(response.body)

    Book.first.poster.attach(io: file, filename: "user_avatar_#{1}.jpg", content_type: "image/jpg")
  end

  def self.utf8_to_ascii(string = "", limit = 100)
    utf8_mapping = {
      "a"=>"áàảãạăắặằẳẵâấầẩẫậ",
      "d"=>"đ",
      "e"=>"éèẻẽẹêếềểễệ",
      "i"=>"íìỉĩịÍÌỈĨỊ",
      "o"=>"óòỏõọôốồổỗộơớờởỡợ",
      "u"=>"úùủũụưứừửữự",
      "y"=>"ýỳỷỹỵ"
    }
    return_str = string.downcase.split(" ").map do |str|
      unless str.ascii_only?
        utf8_mapping.each do |k, v|
          str.gsub!(/[#{v}]/i, k)
        end
      end
      str
    end
    return add3dots(return_str.join(" "), limit)
  end

  def self.float?(string)
    true if Float(string) rescue false
  end

  def self.ecb64(str)
    return Base64.encode64(str.to_s) rescue ""
  end

  def self.dcb64(str)
    return Base64.decode64(str) rescue ""
  end

  def self.add3dots(string, limit)
    dots = "..."
    if string.length > limit
      string = "#{string[0..limit]}#{dots}"
    end
    return string
  end
end
