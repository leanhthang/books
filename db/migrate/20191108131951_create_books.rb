class CreateBooks < ActiveRecord::Migration[6.0]
  def change
    enable_extension 'citext'
    create_table :books do |t|
      t.citext  :title, null: false
      t.citext  :title_search, null: false

      # Origin data
      t.string  :origin_link, null: false
      t.string  :origin_rs, null: false
      t.string  :origin_img

      # References
      t.references :author, foreign_key: true

      t.text  :descr
      t.integer :public, default: false, limit: 1
      t.jsonb  :data, null: false, default: {}
      # finished/ updating
      t.integer :status, limit: 1
      # The currently, who is responsibility for this post
      t.uuid :assigned_to, default: ""
      t.uuid :assigned_by, default: ""

      t.integer :visited, default: 0

      # Extention
      t.string :slug, null: false

      t.timestamps
    end

    add_index :books, :title
    add_index :books, :title_search
    add_index :books, :slug, unique: true
  end
end
