class CreateChaps < ActiveRecord::Migration[6.0]
  def change
    enable_extension 'citext'
    create_table :chaps do |t|
      t.citext  :title, null: false
      t.citext  :title_search
      t.string  :origin_content
      t.string  :content
      t.boolean :public, default: false
      # WHo is translate it
      t.string  :translator, default: false
      # WHo is edit this chapter
      t.string  :editor, default: false
      # who is approve this chapter
      t.string  :approve_by, default: false
      t.text    :description
      t.string  :origin_link
      t.integer :order_c

      t.references    :book, foreign_key: true
      t.timestamps
    end
  end
end
