class BooksForm
  def initialize(params)
    super(params)
  end

  def save
    ActiveRecord::Base.transaction do
      if valid?
      end
    end
  end

  private

  def book

  end

  def category

  end

  def chap

  end

  def author

  end

  def book_params

  end

  def category_params

  end

  def author_params

  end

  def chap_params

  end
end