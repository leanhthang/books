const $cmUI = new function(){
  this.endOfColFirst = null;
  this.firstInit = true;
  this.is_mobile = $util.isMobile();
  this.cmBoxWrapper = '#cmBox-wrapper';
  this.scrollPos = 0;
  this.endOfCol = 0;
  this.currentPage = 1;
  this.originalText = "";
  this.boxWidthState = $(window).width();
  this.currentUrl = location.href;

  this.cstmCSSColumnContext = function(){
    $cmUI.cmBoxWidth = $("#cmBox_body").width();

    $(this.cmBoxWrapper).css({
      '-webkit-column-width': $cmUI.cmBoxWidth + "px",
      '-moz-column-width': $cmUI.cmBoxWidth + "px",
      'column-width': $cmUI.cmBoxWidth + "px"
    });

    userSS.settingInit();

    $cmUI.getBoxWidth();
    $cmUI.countTotalPage();

    // $cmUI.cmBoxWidth = Math.ceil($cmUI.cmScrollWidth/ $cmUI.totalPage);
  }

  this.getBoxWidth = function(){
    // $cmUI.cmScrollWidth = $('#cmPoint-end').position().left;
    $cmUI.cmScrollWidth = $('#cmBox-wrapper')[0].scrollWidth;
    $(window).resize(function(){
      clearTimeout($cmUI.timeoutBoxWith);
      $cmUI.timeoutBoxWith = setTimeout(function(){
        // $cmUI.cmBoxWidth = Math.ceil($cmUI.cmScrollWidth/ $cmUI.totalPage);
        $cmUI.cmScrollWidth = $('#cmBox-wrapper')[0].scrollWidth;
      });
    });
  }

  // START: Click right or left
  this.countTotalPage = function(){
    $cmUI.totalPage = Math.ceil(($cmUI.cmScrollWidth - 10)/$cmUI.cmBoxWidth);
  }

  this.scrollEventMobile = function(){
    let ts;
    let threshold = 30;
    $cmUI.touchState = { cx: 0, cy: 0, direct: 0, distance: 0 };

    $('#cmBox_body').on('touchmove', function (e){
      let tm = e.originalEvent.changedTouches[0].clientX;
      page = $cmUI.currentPage == 0 ? 0 : ($cmUI.currentPage -1);
      scroll = $cmUI.cmBoxWidth * page + ($cmUI.touchState.cx - tm);
      $cmUI.transformClick(scroll, 8);
    });

    $('#cmBox_body').on('touchstart', function (e){
      ts = e.originalEvent.touches[0].clientX;
      $cmUI.touchState.cx = ts;
    });

    $('#cmBox_body').on('touchend touchcancel', function (e){
       let te = e.originalEvent.changedTouches[0].clientX;
      if ($cmUI.touchState.cx < (te + 5)) {
        $cmUI.touchState.direct = 'left';
      } else if ($cmUI.touchState.cx > (te -5)) {
        $cmUI.touchState.direct = 'right';
      } else {
        $cmUI.touchState.direct = 0;
      }
      $cmUI.touchState.distance = Math.abs($cmUI.touchState.cx - te);
      $cmUI.touchState.cx = te;
      if ($cmUI.touchState.direct !== 0) {
        if ($cmUI.touchState.distance > threshold) {
          if ($cmUI.touchState.direct == 'left') {
            $cmUI.leftClick();
          } else if ( $cmUI.touchState.direct == 'right') {
            $cmUI.rightClick();
          }
        } else {
          $cmUI.transformClick(null, 30);
        }
        $cmUI.drawCMFooter();
      } else {
        $cmUI.transformClick(null, 100);
      }
    });
  }

  this.leftClick = function() {
    if ($cmUI.currentPage > 1) {
      $cmUI.currentPage = Math.abs($cmUI.currentPage - 1);
      $cmUI.transformClick(null, 100);
      $cmUI.drawCMFooter();
    }
  }

  this.rightClick = function() {
    if ( $cmUI.currentPage < $cmUI.totalPage ) {
      $cmUI.currentPage = Math.abs($cmUI.currentPage + 1);
      $cmUI.transformClick(null, 100);
      $cmUI.drawCMFooter();
    }
  }

  this.transformClick = function(scrollTo, duration){
    duration = duration || 90;
    page = $cmUI.currentPage == 0 ? 0 : ($cmUI.currentPage - 1);
    $cmUI.scrollPos = scrollTo || page * $cmUI.cmBoxWidth;
    $($cmUI.cmBoxWrapper).animate({scrollLeft: $cmUI.scrollPos}, duration);
  }

  // START: Footer handler
  this.drawCMFooter = function(){
    page_detail = $cmUI.currentPage +"/"+$cmUI.totalPage;
    title = $cmUI.add3Dots($("#chap-title").val())
    read_progress = $("#current-chap").val() +"/"+$("#total-chaps").val()

    chapTitle = "<span class='float-right'>"+'title'+" &nbsp;("+$cmUI.cmScrollWidth+' / '+$cmUI.cmBoxWidth+"; "+  $cmUI.scrollPos+ ")</span>"
    $("#cmBox-footer").html(page_detail+chapTitle)
  }
  // End footer

  // START: Utility
  this.add3Dots = function(string){
    let boxW = $cmUI.cmBoxWidth
    if(boxW < 320 ){
      return "";
    } else if (boxW < 400 ) {
      limit = 27;
    } else if (boxW < 560 ) {
      limit = 40;
    } else if (boxW < 760 ) {
      limit = 55;
    } else {
      limit = 100;
    }
    return $util.add3Dots(string, limit)
  }

  this.fullScreen = function(){
    clearTimeout($cmUI.fullScreenState);
    $cmUI.fullScreenState = setTimeout(function(){
      $util.fullScreen();
      clearTimeout($cmUI.fullScreenUpdateView);
      $cmUI.fullScreenUpdateView = setTimeout(function(){
        $cmUI.getBoxWidth();
        $cmUI.countTotalPage();
        $cmUI.drawCMFooter();
        $cmUI.init();
      }, 300);
      clearTimeout($cmUI.fullScreenState);
    },10);
  }
  // END: Utility

  this.initWhenResize = function() {
    clearTimeout($cmUI.resizeStatus);
    $cmUI.resizeStatus = setTimeout(function(){
      $cmUI.init();
      if ( $cmUI.totalPage < $cmUI.currentPage ) {
        $cmUI.currentPage = $cmUI.totalPage;
      }
      $cmUI.drawCMFooter();
    }, 500);
  }

  this.init = function(){
    $cmUI.cstmCSSColumnContext();
    $cmUI.countTotalPage();
    $cmUI.drawCMFooter();
    $cmUI.scrollEventMobile();
  }
}