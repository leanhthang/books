class Scan::BachNgocSachService < ScanService
  # Note:
  # reload! && Scan::BachNgocSachService.new({begin: 0, pages: 1}).save_link_books
  def initialize(**args)
    super
    @base_path = "https://bachngocsach.com/"

    @books_fetch_items = {
        book_item: ".view-content ul li.book-row .book-wrap", # Book item
        origin_link: '.book-truyen a',
        origin_img: '.book-anhbia-a',
        title: '.book-truyen a'
      }
    @book_dom = { }
    @chap_dom = { }
  end

  def book_html_dom
    {
      item_scan: ".view-content ul li.book-row",
      title: ".book-truyen a",
      #
      description: "#gioithieu",
      origin_img: "#anhbia img",
      author: "#tacgia a",
      categories: '#theloai a',
      types: "#flag .flag-term"
    }
  end

  def chap_html_dom

  end
end