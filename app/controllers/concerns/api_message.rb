module ApiMessage
  extend ActiveSupport::Concern

  def api_msg(i18n_key, params = {})
    I18n.t("api.v1.#{i18n_key}", params)
  end

  def error_i18n(object_name, i18n_key, params = {})
    I18n.t("activerecord.errors.models.#{object_name}.attributes.#{i18n_key}", params)
  end
end
