class Api::V1::BooksController < Api::ApiController
  before_action :set_post, only: [:show, :chapters]

  def show
    other_books = Post.where(author_id: @post.author.id)
    render json: @post, root: 'data',
                        each_serializer: PostSerializer,
                        other_books: other_books
  end

  def chapters
    @chapters = @post.chapters.select(:id, :title, :order_c)
    pagy, chapters = pagy_extended(@chapters, {items: params[:per_page], page: params[:page]})
    render json: chapters, root: 'data',
                        each_serializer: ChapterSerializer,
                        meta: pagy
  end

  private
    def set_post
      @post = Post.find_by(id: params[:id])
      if @post.blank?
        render json: {
            erorrs: true,
            messages: "Không tìm thấy truyện với id `#{params[:id]}`",
            code: 401
          }, status: 401
      end
    end
end
