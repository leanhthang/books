require "google/apis/calendar_v3"
require "google/apis/gmail_v1"
require "googleauth"
require "googleauth/stores/file_token_store"
require "fileutils"
require "mail"

class GoogleService::GoogleApiService

  CREDENTAL_PATH = "#{Rails.root.to_s}/lib/google/client_secret_325927860377-kmqg63665tcs4pvsf7qvnjtbllscvh2g.apps.googleusercontent.com.json".freeze
  SCOPES = ['https://mail.google.com/',
      'https://www.googleapis.com/auth/gmail.send',
      'https://www.googleapis.com/auth/gmail.compose',
      'https://www.googleapis.com/auth/gmail.modify',
      'https://www.googleapis.com/auth/calendar',
      'https://www.googleapis.com/auth/calendar.events'].freeze
  OOB_URI = "urn:ietf:wg:oauth:2.0:oob".freeze

  def authorize
    token_path = "#{Rails.root.to_s}/lib/google/token.yaml"
    client_id = Google::Auth::ClientId.from_file CREDENTAL_PATH
    token_store = Google::Auth::Stores::FileTokenStore.new(file: token_path)
    authorizer = Google::Auth::UserAuthorizer.new client_id, SCOPES, token_store

    user_id = "default"
    credentials = authorizer.get_credentials user_id
    if credentials.nil?
      url = authorizer.get_authorization_url base_url: OOB_URI
      puts "Open the following URL in the browser and enter the " \
           "resulting code after authorization:\n" + url
      code = gets
      credentials = authorizer.get_and_store_credentials_from_code(
        user_id: user_id, code: code, base_url: OOB_URI
      )
    end

    credentials
  end

end
