class ScanerJob < ApplicationJob

  # Resource: BachNgocSachService/ TangThuVien
  def perform(resource, query, params)
    begin
      object = "Scan::#{resource}".constantize
      case query
      when 'save_link_books'
        object.new(params).save_link_books
      when 'save_books_detail'
        object.new(params).save_books_detail
      when 'save_link_chaps'
        object.new(params).save_link_chaps
      end

    rescue Exception => e
      p "======= ScanerJob: #{e} ======="
    end
  end

  private

  def method_name

  end
end