//= require ./libs/jquery-3.3.1.min.js
//= require ./libs/bootstrap.js
//= require ./libs/utility.js
//= require_tree ./controllers
//= require pagy

// Comic
//= require ./clients/audio
//= require ./clients/user_session
//= require ./clients/comic.js
//= require ./clients/comic_toolbox.js

$(function () {
  window.addEventListener("load", Pagy.init);
});