class Api::ApiController < ActionController::API
  # include DeviseTokenAuth::Concerns::SetUserByToken

  # include Api::ErrorHandler
  include Pagy::Backend
  include ApiMessage
  include ApiResponse
  include ErrorHandler
  include ReaderAuthentication

  before_action :set_headers
  before_action :check_headers

  def pagy_extended(collection, vars={})
    pagy, paginated = pagy(collection, vars)
    meta_data = { current_page: pagy.page,
      next_page:    pagy.next,
      prev_page:    pagy.prev,
      total_pages:  pagy.pages,
      total_count:  pagy.count
    }

    return meta_data, paginated
  end

  protected
    def set_headers
      response.headers['API-Version'] = Rails.application.secrets.app_version
    end

    def check_headers
      if request.headers['Device-Id'].blank?
        # device_id, device_token
        render_error("Thiếu headers bắt buộc", 412, :precondition_failed, ['Precondition failed'])
      else
        devise_id = request.headers['Device-Id']
      end
    end
end
