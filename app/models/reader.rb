require 'jwt'
class Reader < ApplicationRecord
  extend Devise::Models

  # acts_as_paranoid
  attr_accessor :login
  attr_accessor :skip_password_validation

  has_one_attached :avatar

  enum gender: {
    male: 1,
    female: 2,
    other: 3
  }

  enum provider: {
    internal: 1,
    google: 2,
    facebook: 3
  }

  devise :database_authenticatable, :registerable, :trackable, :validatable, :authentication_keys => [:login]

  include DeviseTokenAuth::Concerns::User

  validates :provider, uniqueness: { scope: [:email, :phone, :uid, :provider] }
  validates :email, format: { with: /\b[A-Z0-9._%a-z\-]+@(?:[A-Z0-9a-z\-]+\.)+[A-Za-z]{2,4}\z/ }
  validates :uid, uniqueness: { scope: [:uid, :provider] }

  # Callback
  before_save :before_save_email
  before_create :gen_expired_token
  after_create :sync_with_interaction_history

  has_one :interaction

  def gen_jwt
    JWT.encode({ id: id,
                exp: 60.days.from_now.to_i },
               Rails.application.secrets.secret_key_base)
  end

  def self.current
    Thread.current[:user]
  end

  def self.current=(user)
    Thread.current[:user] = user
  end

  def self.find_for_database_authentication(warden_conditions)
    conditions = warden_conditions.dup
    if login = conditions.delete(:login)
      where(conditions.to_h).where(["(lower(phone) = :value AND LENGTH(phone) > 0) OR (lower(email) = :value AND LENGTH(email) > 0)", { :value => login.downcase }]).first
    elsif conditions.has_key?(:phone) || conditions.has_key?(:email)
      where(conditions.to_h).first
    end
  end

  # Skip validate email
  def will_save_change_to_email?
    false
  end

  protected

    def password_required?
      return false if skip_password_validation
      super
    end

  private

    def sync_with_interaction_history
      inter = Interaction.find_or_create_by(device_id: self.device_id)
      inter.reader_id = self.id
      inter.save
    end

    # If user don't have email then auto gen for them
    def before_save_email
      if self.email.blank?
        self.email = "#{SecureRandom.uuid}@temp.exp"
      end
      self.email = email.downcase
    end

    def gen_expired_token
      self.expired_token = self.gen_jwt
    end
end
