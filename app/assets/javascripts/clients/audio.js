const cmAudio = new function() {
  this.currentStatus = '';
  this.audioCurrentIndexStop = 0;
  this.playStatus = '';
  this.isPause = false;
  this.textData = $('#comicData p');

  this.play = function(){
    if (userSS.baseData().displayOption == "swipe") {
      localStorage.setItem("displayOption", "swipe")
      userSS.setDisplayOption('scroll')
    }
    if (cmAudio.playStatus == 'pause') {
      speechSynthesis.cancel();
      speechSynthesis.cancel();
    }
    cmAudio.playStatus = 'speak';
    cmAudio.updateUISetting();

    cmAudio.textData = $('#comicData p');
    $('#comicData .onreading').removeClass('onreading alert-danger');
    $(cmAudio.textData[cmAudio.audioCurrentIndexStop]).addClass('onreading alert-danger');

    // reset display
    localStorage.setItem("audioIsPlay", 'true');

    // Set audio session
    this.audioPitch = parseFloat(userSS.baseData().audioPitch);
    this.audioRate = parseFloat(userSS.baseData().audioRate);
    this.audioLang = userSS.baseData().audioLang;
    speechSynthesis.getVoices();

    textIndex = cmAudio.audioCurrentIndexStop;
    textContent = $(cmAudio.textData[textIndex]).text();
    textContent = cmAudio.skipReading(textContent);

    if (cmAudio.audioLang === ""){
      alert('Xin vui lòng chọn 1 ngôn ngữ');
      return false;
    }

    if (textContent.length === 0) { return 0;}
    let text = new SpeechSynthesisUtterance();
    let voices = speechSynthesis.getVoices();
    text.voice = voices.find(el => el.lang === cmAudio.audioLang );
    text.rate = cmAudio.audioRate;
    text.lang = text.voice.lang;
    text.pitch = cmAudio.audioPitch;
    text.localService = true;
    text.text = textContent;
    text.onend = function(e) {
      cmAudio.audioCurrentIndexStop += 1;
      let textTag = $(cmAudio.textData[cmAudio.audioCurrentIndexStop]);
      let textContent = textTag.text();
      if (textContent.length > 0) {
        cmAudio.updateUISetting();
        cmAudio.play();
      } else {
        localStorage.setItem("audioIsPlay", 'false');
      }
    };
    cmAudio.scrollToTop();
    speechSynthesis.speak(text);
  }

  this.pause = function(){
    cmAudio.playStatus = 'pause';
    cmAudio.isPause = true;
    cmAudio.updateUISetting();
    localStorage.setItem("audioIsPlay", 'false');
    speechSynthesis.pause();
  }

  this.scrollToTop = function(){
    if ($('#comicData .onreading').length > 0) {
      osPos = $('#comicData .onreading').position();
      if ( userSS.baseData().displayOption === 'scroll' ) {
        $('#comicContent').scrollTop(osPos.top + 60);
      } else {
        navPage = Math.round(osPos.left / cmUI.comicBoxWidth);
        cmUI.currentPage = Math.round(osPos.left / cmUI.comicBoxWidth) + 1;
        cmUI.transformClick(10, cmUI.comicBoxWidth * navPage);
      }
    }
  }

  this.skipReading = function(text){
    text = text.replace(/\.{3,}/, '...');
    text = text.replace(/~{3,}/, '~');
    return text;
  }

  this.updateUISetting = function(){
    let ssData = userSS.baseData();
    if (cmAudio.playStatus === 'speak') {
      $('#audio-btn-play').addClass('sr-only');
      $('#audio-btn-pause').removeClass('sr-only');
    } else {
      $('#audio-btn-play').removeClass('sr-only');
      $('#audio-btn-pause').addClass('sr-only');
    }

    // Change reading
    percent = Math.round((cmAudio.audioCurrentIndexStop)*100 / $('#comicData p').length);
    $('#badge-audio-reading-at').html((`${percent} %`));
    $('#audioChangeReading').val(cmAudio.audioCurrentIndexStop);

    // On reading
    if (cmAudio.playStatus == 'speak') {
      $('#audio-btn').removeClass('sr-only');
    } else {
      $('#audio-btn').addClass('sr-only');
    }
  }

  this.updateConf = {
    changePitch: function(_this){
      new_val = _this.value;
      localStorage.setItem("audioPitch", new_val);
      $('#badge-audio-pitch').text(new_val);
    },
    changeSpeed: function(dir_turn){
      let current_val = userSS.baseData().audioRate;
      let new_val = 0;
      if (dir_turn === 'up') {
        new_val = current_val + 0.1;
      } else {
        new_val = current_val - 0.1;
      }
      localStorage.setItem("audioRate", new_val);
      $('#audio-value-speed').text(new_val.toFixed(2));
    },
    changeVolumn: function(dir_turn){
      let current_val = userSS.baseData().audioVolumn;
      let new_val = 0;
      if (dir_turn === 'up') {
        new_val = current_val + 1;
      } else {
        new_val = current_val - 2;
      }
      localStorage.setItem("audioVolumn", new_val);
      $('#audio-value-volumn').text(new_val.toFixed(2));
      $('#audioChangeReading').val(cmAudio.audioCurrentIndexStop);
    },
    changeReading: function(_this){
      let $t = $(_this);
      speechSynthesis.pause();
      speechSynthesis.cancel();
      speechSynthesis.cancel();
      cmAudio.audioCurrentIndexStop = parseInt($t.val()) - 2;
      cmAudio.audioCurrentIndexStop -= 1;
      percent = Math.round((cmAudio.audioCurrentIndexStop + 2) / cmAudio.textData.length)
      $('#badge-audio-reading-at').html(`${percent} %`);
      cmAudio.play();
    },
    reset: function(){
      localStorage.setItem("audioPitch", 1.05);
      localStorage.setItem("audioRate", 2);
      cmAudio.play();
    }
  }

  this.genListLanguage = function () {
    cmAudio.audioLang = userSS.baseData().audioLang;
    speechSynthesis.getVoices();
    setTimeout(function(){
      let html = "<select  class='form-control'>";
      html += `<option >-- Chọn 1 --</option>`;
      speechSynthesis.getVoices().forEach(voice => {
        if (cmAudio.audioLang.length === 0 && (voice.lang === 'vi-VN' || voice.lang === 'vi_VN' ) ){
          html += `<option data-lang-code="${voice.lang}" selected="selected">${voice.name}</option>`;
          cmAudio.audioLang = voice.lang;
          localStorage.setItem("audioLang", cmAudio.audioLang);
        } else {
          html += `<option data-lang-code="${voice.lang}">${voice.name}</option>`;
        }
      });

      html += "</select>";
      $('#setting-select-lang').html(html);

      // if not found vn then set default en
      if ($(`[data-lang-code="${cmAudio.audioLang}"]`).length > 0) {
        $(`[data-lang-code="${cmAudio.audioLang}"]`).attr('selected', 'selected');
      } else {
        firstLang = speechSynthesis.getVoices()[0].lang;
        cmAudio.audioLang = firstLang;
        localStorage.setItem("audioLang", firstLang);
      }

      // onchange
      $('#setting-select-lang select').on('change', function(event) {
        cmAudio.audioLang = $(this).find('option:selected').attr('data-lang-code');
        localStorage.setItem("audioLang", cmAudio.audioLang);
        userSS.baseData().audioLang = cmAudio.audioLang;
      });
    }, 150);
  }

  this.init = function(is_play){
    if ('speechSynthesis' in window) {
      $('#audo-not-support').hide();
      cmAudio.textData = $('#comicData p');
      cmAudio.genListLanguage();

      cmAudio.updateUISetting();
      $('#audioChangeReading').attr('max', cmAudio.textData.length);
      $('#audioChangeReading').val(0);
    } else {
      $('#audo-not-support').removeClass('sr-only');
    }
  }
}
