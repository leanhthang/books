const userSS = new function(){
  this.clear =  function(){
    localStorage.clear();
  }

  this.data = {}

  this.baseData = function(){
    data = {}
    data.currentPage        = localStorage.getItem("post-id") || null
    data.postID        = localStorage.getItem("post-id") || null
    data.chapID        = localStorage.getItem("chapter-id") || null
    data.currentChapOrder = localStorage.getItem("cur-chap-order") || null
    data.margin        = localStorage.getItem("margin") || 20
    data.currentPostChap = localStorage.getItem("currentPostChap") || $cmUI.currentPostChap
    data.footer        = localStorage.getItem("footer") || "show"
    data.wrapperBG    = localStorage.getItem("wrap-bg") || "bg-main-2"
    data.background    = localStorage.getItem("background") || "cm-bg2"
    data.fontSize      = localStorage.getItem("fontSize") || 1;
    data.displayOption = localStorage.getItem("displayOption") || "swipe"
    // Audio
    data.audioPitch = parseFloat(localStorage.getItem("audioPitch")) || 1.05;
    data.audioRate = parseFloat(localStorage.getItem("audioRate")) || 2;
    data.audioCurrentIndexStop = parseInt(localStorage.getItem("audioCurrentIndexStop")) || 0;
    data.audioLang = localStorage.getItem("audioLang") || '';
    data.audioIsPlay = localStorage.getItem("audioIsPlay") || 'false';
    return data
  }

  this.setFontSize = function(increase_or_decrease){
    if(increase_or_decrease == 0) return false;
    increase_or_decrease = increase_or_decrease || -1
    increase_or_decrease = parseInt(increase_or_decrease)
    font_size = parseFloat(userSS.baseData().fontSize)
    if (font_size > 0.9 && font_size < 2.1) {
      font_size += increase_or_decrease*0.1
    } else if (font_size <= 0.9) {
      font_size = 0.91
    } else if (font_size >= 2.1) {
      font_size = 2.1
    }

    localStorage.setItem("fontSize", font_size);
    $($cmUI.cmBoxWrapper+", .setting-site-label").css("font-size", font_size +"rem");
    if (userSS.baseData().displayOption == "swipe") {
      $cmUI.initWhenResize();
    }
  }

  this.setCurrentPostChapId = function(){

  }
  this.setFooter = function(){

  }

  this.setBackground = function(){
    $("[data-bg*='cm-bg']").on('click', function(event) {
      let bg = $(this).attr("data-bg");
      let wbg = $(this).attr("data-wbg");

      // Change style button setting
      $("#setting-modal .btn-circle i").removeAttr('class');
      $(`#setting-modal .btn-circle`).removeClass('border border-success custom-shadow text-success');
      $(`#setting-modal [data-wbg="${wbg}"]`).addClass('border border-success custom-shadow text-success');

      $(`#setting-modal [data-wbg="${wbg}"] i`).addClass('fas fa-check')

      // Store data
      localStorage.setItem("background", bg);
      localStorage.setItem("wrap-bg", wbg);

      // Set backgroud
      $("#chap-ui[class*='cm-bg']").removeAttr('class').addClass('container noselect');
      $("#chap-ui").addClass(bg);

      // Set main backgroup
      $("body").removeAttr('class');
      $("body").addClass(wbg);
    });
  }

  this.setPostChapID = function(){
    // set post and chap id
    if($("#post-id").val() != userSS.baseData().postID){
      localStorage.setItem("post-id", $("#post-id").val())
      console.log("post")
    }else{
      if($cmUI.firstInit == true && userSS.baseData().chapID &&
          userSS.baseData().postID == $("#post-id").val() &&
            $("#chapter-id").val() != userSS.baseData().chapID){
        modalUI.show("#preview-chap-modal")
      }else{
        $cmUI.firstInit = false
        localStorage.setItem("chapter-id", $("#chapter-id").val())
      }
    }
  }

  this.setDisplayOption = function(display_option){
    if (display_option == "swipe") {
      localStorage.setItem('audioIsPlay', 'false')
    }
    localStorage.setItem("displayOption", display_option)
    $cmUI.init()
    window.location.reload(true)
  }

  this.setSettingConf = function(){
    userSS.setFontSize(0)
    userSS.setBackground()
    // userSS.setPostChapID()
  }

  this.settingInit = function(){
    // auto set setting has created
    let ssData = userSS.baseData();
    let bg = ssData.background;
    let wbg = ssData.wrapperBG;
    $("#comicContent").addClass(bg);

    // Set font size
    let fontSize = ssData.fontSize;
    $($cmUI.cmBoxWrapper+", .setting-site-label").css("font-size", fontSize +"em");

    // background
    userSS.setBackground();
    $("body").removeAttr('class');
    $("body").addClass(wbg);
    $("#chap-ui").addClass(bg);
    $(`#setting-modal .btn-circle i`).removeAttr('class');
    $(`#setting-modal .btn-circle`).removeClass('border border-success custom-shadow text-success');
    $(`#setting-modal [data-wbg="${wbg}"]`).addClass('border border-success custom-shadow text-success')
    $(`#setting-modal [data-wbg="${wbg}"] > i`).addClass('fas fa-check');

    // Audio session
    $('#audio-value-speed').text(ssData.audioRate.toFixed(2));
    $('#badge-audio-pitch').text(ssData.audioPitch.toFixed(2));
    $('#audioPitch').val(ssData.audioPitch);
    cmAudio.updateUISetting();

    // Set page view
    displayOption = ssData.displayOption;
    if(displayOption == "swipe"){
      $(".setting-view .setting-view-swipe").removeClass('btn-outline-secondary').addClass('btn-danger');
    } else {
      $(".setting-option .setting-view-scroll").removeClass('btn-outline-secondary').addClass('btn-danger');
    }
  }
}
