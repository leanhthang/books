module ApiHelper
  extend ActiveSupport::Concern

  def parse_field_login(login)
    return_field = {}
    if login.scan(/\b[A-Z0-9._%a-z\-]+@(?:[A-Z0-9a-z\-]+\.)+[A-Za-z]{2,4}\z/).present?
      return { email: login }
    end

    if login.scan(/(?:\+?|\b)[0-9]{10,14}\b/).present?
      return { phone: login }
    end

    return false
  end
end
