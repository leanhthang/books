Rails.application.routes.draw do
  require 'sidekiq/web'

  root to: "clients/books#index"

  devise_for :users, path: '/', :controllers => {
      :sessions => 'users/sessions',
      :registrations => 'users/registrations',
      :passwords => 'users/passwords',
    }

  namespace :admin do
    get '/', to: 'admin/home#index'
  end

  namespace :clients, path: '/' do
    resources :books, only: [:index, :show]
    resources :chaps, only: [:index, :show]
  end

  namespace :api do
    namespace :v1 do
      # API login
      mount_devise_token_auth_for 'Reader', at: 'auth', controllers: {
        sessions: "api/v1/auth/sessions",
        registrations: "api/v1/auth/registrations"
      }

      namespace :interactions do
        put :update
        post :add
        get :first_fetch
        delete :remove
      end

      resource :reader, only: [] do
        member do
          put :update_profile
        end
      end

      resources :books, only: [:show, :index]

      resources :chapters, only: [:show]
    end
  end

  scope :monitoring do
    # if Rails.env.production?
      Sidekiq::Web.use Rack::Auth::Basic do |username, password|
        ActiveSupport::SecurityUtils.secure_compare(
          ::Digest::SHA256.hexdigest(username),
          ::Digest::SHA256.hexdigest(Rails.application.credentials[Rails.env.to_sym][:sidekiq][:username] )) & \
        ActiveSupport::SecurityUtils.secure_compare(
          ::Digest::SHA256.hexdigest(password),
          ::Digest::SHA256.hexdigest(Rails.application.credentials[Rails.env.to_sym][:sidekiq][:password] ))
      end
    # end

    mount Sidekiq::Web, at: '/sidekiq'
  end

end
