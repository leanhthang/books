class CreateAuthors < ActiveRecord::Migration[6.0]
  def change
    enable_extension 'citext'
    create_table :authors do |t|
      t.citext :name, null: false
      t.citext :name_vn, null: false
      t.citext :description
      t.integer :book_count, limit: 1

      t.timestamps
    end
  end
end
