require 'nokogiri'
require 'open-uri'
class ScanService
  RESOURCES_PATH = {
    "Scan::BachNgocSachService" => "https://bachngocsach.com/reader/truyen?page=",
    "Scan::Tangthuvien" => "https://truyen.tangthuvien.vn/bang-xep-hang?selOrder=view_&category=0&selComplete=0&selTime=all&page=",
    "Scan::Truyenfull" => "http://truyenfull.vn/danh-sach/truyen-moi/trang-",
    "Scan::Truyencv" => "https://truyencv.com/huyen-ao/trang-1",
  }

  def initialize(**args)
    @books_path = RESOURCES_PATH[self.class.name]
    @beign = args[:begin]
    @pages = args[:pages]
    @books_fetch_items = {}
    @book_dom = {}
    @chap_dom = {}
  end

  def save_link_books
    dom = @books_fetch_items
    title = ''
    @pages.times do |page|
      @beign += 1
      link = ("#{@books_path}#{@beign}").gsub(/\"/,"\"")
      puts link
      docs = get_html(link)

      docs.search(dom[:book_item]).each do |item|
        title = item.at(dom[:title]).text
        Book.find_or_create_by(
            origin_rs: self.class.name,
            origin_link: "#{@base_path}#{item.at(dom[:origin_link])['href']}",
            title: title,
            title_search: Utility.utf8_to_ascii(title),
            origin_img: "#{@base_path}#{item.at(dom[:origin_img])['href']}"
          )
      end
    end
  end

  def save_link_chaps

  end

  def save_books_detail

  end

  private

    def get_book_data
      # @docs.search(@books_dom[:book_item]).each do |doc|
      #   begin
      #     book = BooksForm.new({
      #                 book: {
      #                   title:
      #                   origin_link:
      #                   origin_rs: self.class.name,
      #                   origin_img:
      #                 },
      #                 author: {
      #                   name:
      #                 },
      #                 category: {
      #                   name: []
      #                 }
      #               }).save
      #   rescue Exception => e
      #     puts e
      #   end
      # end
    end

    def get_chap_data

    end

    def get_html(link)
      open_link = open(link,
          "Pragma" => "no-cache",
          "User-Agent" => "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36",
          "X-Requested-With" => "XMLHttpRequest"
        )
      return Nokogiri::HTML(open_link, nil, Encoding::UTF_8.to_s)
    end
end