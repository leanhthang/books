class Api::V1::InteractionsController < Api::ApiController
  before_action :set_interaction, only: [:update, :add, :first_fetch, :remove]

  def first_fetch
    return_data = {}
    list_posts = Post.where(id: @interaction.data['watched'])
                     .select(:id, :title, :origin_img)
                     .order(:title)

    return_data[:watched] = list_posts
    return_data[:favourite] = list_posts.select{|x| x[:id].in?(@interaction.data['favourite']) }

    render_result("Thành công", return_data)
  end

  def add
    case inter_params[:type]
    when 'watched'
      @interaction.data['watched'] << inter_params[:post_id].to_i
      @interaction.data['watched'].uniq!
    when 'favourite'
      @interaction.data['favourite'] << inter_params[:post_id].to_i
      @interaction.data['favourite'].uniq!
    end
    @interaction.save!

    msg = "Thêm thành công (#{inter_params[:type]} => #{inter_params[:post_id]})"
    render_result(msg)
  end

  def remove
    case inter_params[:type]
    when 'watched'
      @interaction.data['watched'] - Oj.load(inter_params[:post_id])
    when 'favourite'
      @interaction.data['favourite'] - Oj.load(inter_params[:post_id])
    end
    @interaction.save!

    msg = "Xóa thành công (#{inter_params[:type]} => #{inter_params[:post_id]})"
    render_result(msg)
  end

  def update
    @interaction.data['favourite'] = Oj.load(interaction_params[:favourite]) if interaction_params[:favourite]
    @interaction.data['watched'] = Oj.load(interaction_params[:watched]) if interaction_params[:watched]
    @interaction.save!

    msg = "Update thành công"
    render_result(interaction_params, msg)
  end

  private
    def set_interaction
<<<<<<< Updated upstream
      device_id = request.headers['Device-Id']

      @interaction = Interaction.where(device_id: device_id).first_or_create!
=======
      token = request.headers['Token']
      devise_id = request.headers['Device-Id']
      @interaction = Interaction.find_or_create_by(token: token, devise_id: devise_id)
>>>>>>> Stashed changes

      if @interaction.data.blank?
        @interaction.data = {watched: [], favourite: []}
        @interaction.save!
      end
      @interaction
    end

    def interaction_params
      return_params = params.require(:interaction).permit(:favourite, :watched)
    end

    def inter_params
      return_params = params.require(:interaction).permit(:type, :post_id)
    end
end
