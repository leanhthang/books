module JsonWebToken
  SECRET_KEY =  Rails.application.secrets.secret_key_base
  class << self
    def encode(payload)
      JWT.encode(payload, SECRET_KEY)
    end

    def decode(token)
      return JWT.decode(token, SECRET_KEY)[0]
    rescue
      nil
    end
  end
end
