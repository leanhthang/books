source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 6.0.0'
# Use postgresql as the database for Active Record
gem 'pg', '>= 0.18', '< 2.0'
gem 'pg_search'
gem 'storext'
gem 'acts_as_paranoid'

# Assets
gem 'sass-rails', '~> 5'
gem 'turbolinks', '~> 5'
gem 'uglifier'

gem 'pagy', '~> 3.4', '>= 3.4.1'
gem 'friendly_id', '~> 5.3'

# Run background task
gem 'sidekiq'
gem 'sidekiq-scheduler'

# Use Redis
gem 'redis', '~> 4.0'

# Auth
gem 'devise', '~> 4.7', '>= 4.7.1'
gem 'devise_token_auth', '~> 1.1', '>= 1.1.3'
gem 'omniauth', '~> 1.8', '>= 1.8.1'
gem "rack-timeout", require:"rack/timeout/base"

# Google API
gem 'google-api-client'

# API
gem 'rack-cors'
gem 'meta-tags', '~> 2.10'
gem 'storext'
gem 'searchkick', '~> 3.1', '>= 3.1.2'
gem 'active_model_serializers'
gem 'faraday'

# Use ActiveStorage variant
gem "aws-sdk-s3", require: false
gem 'image_processing', '~> 1.2'

# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '>= 1.4.2', require: false

# Sercurity
gem 'rack-attack', '~> 6.1'

group :development, :test do
  gem 'puma', '~> 3.11'
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
end

group :development do
  # Access an interactive console on exception pages or by calling 'console' anywhere in the code.
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '>= 3.0.5', '< 3.2'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

group :test do
  # Adds support for Capybara system testing and selenium driver
  gem 'capybara', '>= 2.15'
  gem 'selenium-webdriver'
  # Easy installation and use of web drivers to run system tests with browsers
  gem 'webdrivers'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
