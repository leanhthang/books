class Category < ApplicationRecord
  has_and_belongs_to_many :books, dependent: :delete_all

  has_many :book
end
