# frozen_string_literal: true

class DeviseCreateReaders < ActiveRecord::Migration[6.0]
  def change
    create_table(:readers) do |t|
      ## Required
      t.string  :email
      t.string  :phone, null: false, default: "", :minimum => 9, :maximum => 12
      t.integer  :provider, :null => false, :default => 1
      t.string   :uid, :default => ""
      t.string :device_id, null: false
      t.boolean :verify_user, default: false, null: false

      # Password
      t.string :encrypted_password, null: false, default: ""

      ## Recoverable
      t.string   :reset_password_token
      t.datetime :reset_password_sent_at

      ## Trackable
      t.integer  :sign_in_count, default: 0, null: false
      t.datetime :current_sign_in_at
      t.datetime :last_sign_in_at
      t.inet     :current_sign_in_ip
      t.inet     :last_sign_in_ip

      ## User Info
      t.string    :expired_token, null: false
      t.string    :full_name
      t.integer   :gender, default: 0, limit: 1
      t.datetime  :birthday

      t.datetime  :deleted_at

      ## Tokens
      t.jsonb :tokens, null: false, default: {}

      t.timestamps
    end

    add_index :readers, [:uid, :provider],      unique: true
    add_index :readers, :device_id
    add_index :readers, :reset_password_token, unique: true
  end
end
