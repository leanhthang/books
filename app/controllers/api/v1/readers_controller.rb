class Api::V1::ReadersController < Api::ApiController

  before_action :current_reader
  before_action :update_params, only: [:update_profile]

  def update_profile
    begin
      if @reader.update(update_params)
        render_object(api_msg('sign_in.success'), @reader, ReaderSerializer)
      else
        render_result(api_msg('update_profile.failure'), {}, false, 403)
      end
    rescue Exception => e
      render_result(e, {}, false, 422)
    end
  end

  private

  def update_params
    @new_params ||= params.require(:reader).permit(:gender, :email, :phone, :gender, :full_name, :avatar)

    if @new_params[:email] && @new_params[:provider].in?(['google', 'facebook'])
      @new_params.delete(:email)
    end
    @new_params
  end
end