# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_11_25_091038) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "citext"
  enable_extension "pgcrypto"
  enable_extension "plpgsql"
  enable_extension "uuid-ossp"

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "authors", force: :cascade do |t|
    t.citext "name", null: false
    t.citext "name_vn", null: false
    t.citext "description"
    t.integer "book_count", limit: 2
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "books", force: :cascade do |t|
    t.citext "title", null: false
    t.citext "title_search", null: false
    t.string "origin_link", null: false
    t.string "origin_rs", null: false
    t.string "origin_img"
    t.bigint "author_id"
    t.text "descr"
    t.integer "public", limit: 2, default: 0
    t.jsonb "data", default: {}, null: false
    t.integer "status", limit: 2
    t.uuid "assigned_to"
    t.uuid "assigned_by"
    t.integer "visited", default: 0
    t.string "slug", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["author_id"], name: "index_books_on_author_id"
    t.index ["slug"], name: "index_books_on_slug", unique: true
    t.index ["title"], name: "index_books_on_title"
    t.index ["title_search"], name: "index_books_on_title_search"
  end

  create_table "books_categories", force: :cascade do |t|
    t.bigint "book_id"
    t.bigint "category_id"
    t.index ["book_id"], name: "index_books_categories_on_book_id"
    t.index ["category_id"], name: "index_books_categories_on_category_id"
  end

  create_table "categories", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.boolean "public", default: false
    t.integer "book_count", default: 0
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "chaps", force: :cascade do |t|
    t.citext "title", null: false
    t.citext "title_search"
    t.string "origin_content"
    t.string "content"
    t.boolean "public", default: false
    t.string "translator", default: "f"
    t.string "editor", default: "f"
    t.string "approve_by", default: "f"
    t.text "description"
    t.string "origin_link"
    t.integer "order_c"
    t.bigint "book_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["book_id"], name: "index_chaps_on_book_id"
  end

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string "slug", null: false
    t.integer "sluggable_id", null: false
    t.string "sluggable_type", limit: 50
    t.string "scope"
    t.datetime "created_at"
    t.index ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true
    t.index ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type"
    t.index ["sluggable_type", "sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_type_and_sluggable_id"
  end

  create_table "readers", force: :cascade do |t|
    t.string "email"
    t.string "phone", default: "", null: false
    t.integer "provider", default: 1, null: false
    t.string "uid", default: ""
    t.string "device_id", null: false
    t.boolean "verify_user", default: false, null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.string "expired_token", null: false
    t.string "full_name"
    t.integer "gender", limit: 2, default: 0
    t.datetime "birthday"
    t.datetime "deleted_at"
    t.jsonb "tokens", default: {}, null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["device_id"], name: "index_readers_on_device_id"
    t.index ["reset_password_token"], name: "index_readers_on_reset_password_token", unique: true
    t.index ["uid", "provider"], name: "index_readers_on_uid_and_provider", unique: true
  end

  create_table "users", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.citext "email", default: "", null: false
    t.string "phone", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.date "birthday", null: false
    t.integer "genger", limit: 2, default: 0
    t.citext "full_name"
    t.string "role", limit: 10, default: "user"
    t.boolean "enable", default: false
    t.jsonb "setting", default: {}, null: false
    t.string "provider"
    t.jsonb "detail", default: {}, null: false
    t.integer "status", limit: 2, default: 0
    t.string "oauth_token"
    t.datetime "oauth_expires_at"
    t.string "app_token", limit: 32
    t.jsonb "user_setting", default: {}, null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["phone"], name: "index_users_on_phone", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "books", "authors"
  add_foreign_key "chaps", "books"
end
