# frozen_string_literal: true

class DeviseCreateUsers < ActiveRecord::Migration[6.0]
  def change
    enable_extension 'uuid-ossp'
    enable_extension 'pgcrypto'
    enable_extension 'citext'

    create_table :users, id: :uuid do |t|
      ## Database authenticatable
      t.citext :email,              null: false, default: ""
      t.string :phone,              null: false, default: "", :minimum => 8, :maximum => 12

      t.string :encrypted_password, null: false, default: ""

      ## Recoverable
      t.string   :reset_password_token
      t.datetime :reset_password_sent_at

      ## Rememberable
      t.datetime :remember_created_at

      ## Trackable
      t.integer  :sign_in_count, default: 0, null: false
      t.datetime :current_sign_in_at
      t.datetime :last_sign_in_at
      t.inet     :current_sign_in_ip
      t.inet     :last_sign_in_ip

      t.date   :birthday, null: false
      # t.attachment :avatar
      t.integer  :genger, default: 0, limit: 1

      t.citext    :full_name
      # admin, editor, user
      t.string    :role, limit: 10, default: 'user'
      t.boolean   :enable, default: false
      t.jsonb     :setting, null: false, default: {}

      t.string    :provider
      t.jsonb     :detail, null: false, default: {}

      # t.string  :permission, default: '00000', limit: 1
      t.integer  :status, default: 0, limit: 1

      t.string    :oauth_token
      t.datetime  :oauth_expires_at
      t.string    :app_token, limit: 32

      t.jsonb     :user_setting, null: false, default: {}

      t.timestamps null: false
    end

    add_index :users, :phone,                unique: true
    add_index :users, :email,                unique: true
    add_index :users, :reset_password_token, unique: true
  end
end
