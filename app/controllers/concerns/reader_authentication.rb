module ReaderAuthentication
  extend ActiveSupport::Concern

  included do
    def current_reader
      uid = request.headers['uid']
      token = request.headers['access-token']
      client = request.headers['client']

      @reader = uid && Reader.find_by(uid: uid)
      if @reader && @reader.valid_token?(token, client)
        return true
      else
        render_result("UnauthorizedError", {}, false, 401)
      end
    end
  end
end