// Comic toolbox
const $cmTB = {
  boxID: '#cmBox-wrapper',
  showBasedOnDevice: function(){
    let isMobile = $util.isMobile();
    let isAndroid = $util.isAndroid();
    let isChrome = $util.isChrome();
    let viewSupportDevice = $('[data-device]');
    $.each(viewSupportDevice, function(index, el) {
      let supportArr = $(el).data('device').split(',');
      let isShow = false;

      $.each(supportArr, function(idx2, val2) {
        // debugger
        switch(val2) {
          case 'android':
            if (isAndroid) { isShow = true; }
            break;
          case 'chrome':
            if (isChrome) { isShow = true; }
            break;
          case 'mobile':
            if (isMobile) { isShow = true; }
            break;
          case 'android-chrome':
            if (isMobile && isChrome) { isShow = true; }
            break;
        };
      });
      if (isShow == false) { $(el).addClass('d-none'); }
    });
  },
  showChapList: function() {
    if ($('#chap-list').hasClass('d-none')) {
      $('#chap-list').removeClass('d-none');
    } else {
      $('#chap-list').addClass('d-none');
    }
    $('#cmBox_mask, #cmBox_header, #cmBox_footer').addClass('d-none');
  },
  toggle: function() {
    $(`${$cmTB.boxID}, #cmBox_mask`).on('click', function() {
      if ($('#cmBox_mask').hasClass('d-none')) {
        $('#cmBox_mask, #cmBox_header, #cmBox_footer').removeClass('d-none');
      } else {
        $('#cmBox_mask, #cmBox_header, #cmBox_footer').addClass('d-none');
      }
    });
  },
  // Type: setting/audio/chap_list
  modal: {
    setting: function() {
      $('#cmBox-modal').modal("toggle");
      $('#cmBox-modal .modal-body > div').addClass('d-none');
      $('#cmBox-modal #setting-modal').removeClass('d-none');
    },
    audio: function() {
      $('#cmBox-modal').modal("toggle");
      $('#cmBox-modal .modal-body > div').addClass('d-none');
      $('#cmBox-modal #audio-modal').removeClass('d-none');
    }
  },
  init: function() {
    $cmTB.toggle();
    $cmTB.showBasedOnDevice();
  }
}