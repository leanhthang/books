class Api::V1::Auth::SessionsController < DeviseTokenAuth::SessionsController
  include DeviseTokenAuth::Concerns::SetUserByToken
  include ApiMessage
  include ApiResponse
  include ApiHelper

  before_action :sign_in_params, only: :create
  before_action :fetch_reader, only: :create
  before_action :configure_permitted_parameters
  skip_before_action :verify_authenticity_token

  # sign in
  def create
    if @resource
      client_id, token, expiry = @resource.create_token
      new_auth_header = @resource.build_auth_header(token, client_id)

      # update response with the header that will be required by the next request
      response.headers.merge!(new_auth_header)

      @resource.save
      # reader = uid && Reader.dta_find_by(uid: uid)
      # if reader.valid_token?(token, client_id)
      # end
      sign_in(:reader, @resource)

      data = { reader: @resource }
      msg_success = api_msg('sign_in.success')
      if @resource.provider == 'internal' && parse_field_login(@new_params[:login]).has_key?(:email) && @resource.verify_user == false
        email_content = render_to_string('mails/reader_verify.html.erb', layout: 'mailer').html_safe
        GoogleGmailJob.perform_later( reader: @resource, content: email_content)
        msg_success += '. Please check your email to active account'
      end

      render_object(msg_success, @resource, ReaderSerializer)
    else
      render_result(api_msg('sign_in.unauthorized'), {}, false)
    end
  end

  private

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_in, keys: [:login, :uid, :provider, :password])
  end

  def sign_in_params

    @new_params = params.require(:auth).permit(:login, :uid, :provider, :password)

    if @new_params[:provider] && !@new_params[:provider].in?(['google', 'facebook'])
      render_result('Please check your input, wrong provider', {}, false, 422)
    end

    # If login with password
    if @new_params[:provider].blank? && @new_params[:password].blank?
      render_result('Please check your input, API should require password', {}, false, 422)
    # Social network login
    elsif @new_params[:provider].present? && @new_params[:uid].blank?
      render_result('Please check your input, API should require the social network uid', {}, false, 422)
    end

    # Auto fill password_confirmation
    if @new_params[:password]
      @new_params.merge!({password_confirmation: @new_params[:password]})
    else
      @new_params.merge!({password: 'social-network-provider', password_confirmation: 'social-network-provider'})
    end

    # set uid
    unless @new_params.has_key?(:uid)
      @new_params.merge!({uid: @new_params[:login]})
    end

    # Set provider
    unless @new_params[:provider]
      @new_params[:provider] = 'internal'
    end
    # Add device ID
    @new_params.merge!({device_id: request.headers['Device-Id'] })

    # Set email, phone
    login_parse = parse_field_login(@new_params[:login])
    if login_parse
      @new_params.merge!(login_parse)
      @new_params
    else
      render_result(api_msg('sign_up.email_phone_missing'), {}, false, 422)
    end

    @new_params
  end

  # Output:
  # success status, resource/ messages
  def fetch_reader
    @resource = Reader.find_for_database_authentication(login: @new_params[:login], provider: @new_params[:provider])

    if @resource
      if @new_params[:password] != 'social-network-provider' && !@resource.valid_password?(@new_params[:password])
        render_result('Wrong password!', {}, false, 422)
      end
    else
      if Reader.find_for_database_authentication(login: @new_params[:login], verify_user: true) || @new_params[:provider].in?(['google', 'facebook'])
        @new_params[:verify_user] = true
      end
    end

    unless @resource
      # Create new user
      @resource = Reader.new @new_params

      begin
        if @resource.save
          return @resource
        else
          msg = "#{api_msg('sign_in.failure')}: #{@resource.errors.full_messages.join(', ')}"
          render_result(msg, {}, false)
        end
      rescue Exception => e
        render_result(e, {}, false)
      end
    end
  end
end
