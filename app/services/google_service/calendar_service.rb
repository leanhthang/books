module GoogleService
  class CalendarService < GoogleApiService
    def add_calendar
      begin_date = (Time.now + 9.hours).strftime("%FT%T%:z")
      end_date = (Time.now + 10.hours).strftime("%FT%T%:z")

      # Authorization
      calendar = Google::Apis::CalendarV3::CalendarService.new
      calendar.client_options.application_name = 'nhatro_google_api'
      calendar.authorization = authorize

      # email receive
      return_emails = [{ email: 'leanhthang70@gmail.com' }]

      # Add event
      event = Google::Apis::CalendarV3::Event.new({
          summary: 'Test summary',
          # location: '800 Howard St., San Francisco, CA 94103',
          description: 'Test description',
          start: { date_time: begin_date },
          end: { date_time: end_date },
          attendees: return_emails
        })

      # Reminder one hours before the event takes place
      event.reminders = Google::Apis::CalendarV3::Event::Reminders.new(
        use_default: false,
        overrides: [
          Google::Apis::CalendarV3::EventReminder.new(reminder_method: "popup", minutes: 60),
          Google::Apis::CalendarV3::EventReminder.new(reminder_method: "email", minutes: 60)
        ]
      )

      # Send calendar
      calendar.insert_event('primary', event)
    end
  end
end