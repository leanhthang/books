module GoogleService
  class MailService
    def send_email(user, content)
      authorize = GoogleService::GoogleApiService.new.authorize

      mail = Mail.new
      mail.to    = user.email
      mail.subject = 'Test email kich hoat tai khoan'
      mail.body    =  content

      system "curl --request POST \
        'https://www.googleapis.com/gmail/v1/users/me/messages/send?alt=json&key=240f9ce941516e79222d2d4084602f264f72cdc0' \
        --header 'Authorization: Bearer #{authorize.access_token}' \
        --header 'Accept: application/json' \
        --header 'Content-Type: application/json' \
        --data '{\"raw\":\"#{Base64.urlsafe_encode64(mail.to_s)}\"}' \
        --compressed"
    end
  end
end